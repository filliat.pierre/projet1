<?php

namespace App\Tests;

use App\Entity\Blogpost;
use DateTime;
use PHPUnit\Framework\TestCase;

class BlogpostUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $blogpost = New Blogpost();
        $datetime = New DateTime();

        $blogpost->setTitre(('titre'))
                 ->setCreatedAt($datetime)
                 ->setContenu('contenu')
                 ->setSlug('slug');

        $this->assertTrue($blogpost->getTitre() === 'titre');
        $this->assertTrue($blogpost->getCreatedAt() === $datetime);
        $this->assertTrue($blogpost->getContenu() === 'contenu');
        $this->assertTrue($blogpost->getSlug() === 'slug');
    }

    public function testIsFalse()
    {
        $blogpost = New Blogpost();
        $datetime = New DateTime();

        $blogpost->setTitre(('titre'))
                 ->setCreatedAt($datetime)
                 ->setContenu('contenu')
                 ->setSlug('slug');

        $this->assertFalse($blogpost->getTitre() === 'false');
        $this->assertFalse($blogpost->getCreatedAt() === new DateTime());
        $this->assertFalse($blogpost->getContenu() === 'false');
        $this->assertFalse($blogpost->getSlug() === 'false');
    }

    public function testIsEmpty()
    {
        $blogpost = new Blogpost();
        
        $this->assertEmpty($blogpost->getTitre() === 'false');
        $this->assertEmpty($blogpost->getCreatedAt() === new DateTime());
        $this->assertEmpty($blogpost->getContenu() === 'false');
        $this->assertEmpty($blogpost->getSlug() === 'false');
    }
}
